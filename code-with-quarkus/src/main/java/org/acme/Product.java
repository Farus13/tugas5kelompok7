package org.acme;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class Product {

    @NotBlank(message = "Name tidak boleh kosong")
    public String name;

    @NotBlank(message = "Brand tidak boleh kosong")
    public String brand;

    @NotBlank(message = "Type tidak boleh kosong")
    public String type;

    @Max(message = "Maximal quantity adalah 10", value = 10)
    @Min(message = "Minimal quantity adalah 4", value = 4)
    public Integer quantity;

    public Product(String name, String brand, String type, Integer quantity) {
        this.name = name;
        this.brand = brand;
        this.type = type;
        this.quantity = quantity;

    }










}
